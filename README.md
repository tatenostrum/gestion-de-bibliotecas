#**Gesti�n de Bibliotecas**

Aqui podreis encontrar el c�digo completo y comentado de la p�gina web.

*Podeis verlo directamente online desde esta pagina o clonarlo a tu equipo con el comando* `git -clone https://bitbucket.org/tatenostrum/gestion-de-bibliotecas/`

---

##Requisitos para el buen funcionamiento de la p�gina web

En la plataforma Windows con la instalaci�n del servidor web *XAMPP* tenemos suficiente con clonar el proyecto en la carpeta htdocs. Con el *XAMPP* ya viene incluido el servidor *MySQL* y el *PHPMyAdmin*. En la plataforma Ubuntu necesitamos tener instalado el servidor web apache y tener un servidor *MySQL* instalado y para una configuraci�n mas comoda recomiendo instalar el *PHPMyAdmin*.

Para instalar el servidor *MySQL* y el *PHPMyAdmin* lo hacemos con el siguiente comando:

`sudo apt install mysql-server phpmyadmin`

---
##Instalación

Primero debemos iniciar la base de datos y cargar el script proporcionado en el repositorio.

Cuando est� creada la base de datos con las tablas y los datos insertados por defecto, configuramos el host, el usuario y la contrase�a de la base de datos en config/database.php y en el archivo api/login.php. Para que se conecte correctamente a la base de datos.

Y luego copiamos el repositorio directamente en el servidor web.

##Cargar SQL a la base de datos

Para cargar el script SQL proporcionado en el repositorio para la base de datos lo podemos hacer de dos formas

Si disponemos de phpMyAdmin con la opcion de importar, le cargamos el archivo proporcionado.

En cambio si estamos utilizando la consola de MySQL podemos cargar el script con el comando

`source C:\rutadelarchivo.sql`

