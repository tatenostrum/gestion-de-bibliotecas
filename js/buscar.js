$("#search").bind('input', ()=>{
    if($("#search").val()==""){
        $("#result").html("")
        return false
    }
    if($("#search").hasClass("autor")||$("#search").hasClass("libautor")){
        var ruta="api/autor/search.php"
        var datas={"nombre":$("#search").val()}
        var table="<tr><th>IDautor</th><th>Nombre</th></tr>"
    }else if($("#search").hasClass("libro")){
        var ruta="api/libro/search.php"
        var datas={"isbn":$("#search").val()}
        var table="<tr><th>ISBN</th><th>Titulo</th><th>Autor</th><th>Editorial</th></tr>"
    }

    $.post(ruta,datas,(res)=>{
        if($("#search").hasClass("libro")){
            for(let i of res.resultados){
                table+='<tr><td id="ckl">'+i.isbn
                table+="</td><td>"+i.titulo
                table+="</td><td>"+i.autor
                table+="</td><td>"+i.editorial
                table+="</td></tr>"
            }
        }else if($("#search").hasClass("autor")||$("#search").hasClass("libautor")){
            for(let i of res.resultados){
                table+='<tr><td id="cka">'+i.idautor
                table+="</td><td>"+i.nombre
                table+="</td></tr>"
            }
        }
        $("#result").html(table)
    })
})

$("#result").on('click','tr',function(){
    if($("#search").hasClass("autor")){
        var datas={
            "idautor":$(this).children().first().text()
        }
        var ruta="api/autor/readOne.php"
    }else if($("#search").hasClass("libro")){
        var datas={
            "isbn":$(this).children().first().text()
        }
        var ruta="api/libro/readOne.php"
    }else if($("#search").hasClass("libautor")){
        var idautor=$(this).children().first().text()
        $("#Autor").val(idautor)
    }
    $.get(ruta,datas, function(res){
        if($("#search").hasClass("autor")){
            $("#idautor").text(res.idautor)
            $("#nombre").val(res.nombre)
            $("#fecha").val(res.fecha)
        }else if($("#search").hasClass("libro")){
            $("#ISBN").val(res.isbn)
            $("#Titulo").val(res.titulo)
            $("#Edicion").val(res.edicion)
            $("#Editorial").val(res.editorial)
            $("#Genero").val(res.genero)
            $("#Tapa").val(res.tapa)
            $("#Sinopsis").val(res.sinopsis)
            $("#Fechap").val(res.fechap)
            $("#Fechae").val(res.fechae)
        }else if($("#search").hasClass("libro") && $("#btn-autor").hasClass("borrar")){
            $("#ISBN").val(res.isbn)
        }
    })
})