$("#btn-autor").click(()=>{
    var ruta;
    if($("#nombre").val()==""){
        return false;
    }else{
        if(!$(this).hasClass("borrar")&&$("#autor")==""){
            return false;
        }
    }
    if($("#btn-autor").hasClass("insertar")){
        ruta="api/autor/create.php";
        var datas={
            "nombre":$("#nombre").val(),
            "fecha":$("#fecha").val()
        }
    }
    if($("#btn-autor").hasClass("modificar")){
        ruta="api/autor/update.php";
        var datas={
            "idautor":$("#idautor").text(),
            "nombre":$("#nombre").val(),
            "fecha":$("#fecha").val()
        }
    }
    if($("#btn-autor").hasClass("borrar")){
        ruta="api/autor/delete.php";
        var datas={
            "idautor":$("#idautor").text()
        }
    }
    console.log(ruta)
    $.post(ruta,datas,(res)=>{
        alert(res.message);
        if(res.message.indexOf("no")==-1){
            $("#idautor").text("")
            $("#nombre").val("")
            $("#fecha").val("")
            $("#search").val("")
            $("#result").html("")
        }
    })
})