
$("document").ready(function(){
    $("#sel").trigger("change")

    console.log("Ok");
})
$("#sel").change(()=>{
    let option=$("#sel option:selected").text();
    let ruta="";
    
    if(option == "Libros"){
        ruta = "api/libro/read.php";
    }else if(option == "Autor"){
        ruta = "api/autor/read.php";
    }else if(option == "Mi biblioteca"){
        ruta = "api/biblioteca/read.php";
    }
    console.log(ruta);
    $.get(ruta,(dat)=>{
        if(option=="Libros"){
            let table="<tr><th>ISBN</th><th>Titulo</th><th>Autor</th><th>Editorial</th><th>Genero</th></tr>"
            for(let i of dat.resultados){
                table += "<tr><td>" + i.ISBN 
                table +="</td><td>" + i.titulo 
                table += "</td><td>" + i.autor
                table += "</td><td>"+ i.editorial
                table += "</td><td>"+ i.genero
                table += "</td></tr>"
            }
            $("#result").html(table);
        }else if(option=="Autor"){
            let table ="<tr><th>Autor</th><th>Fecha</th></tr>"
            for(let i of dat.resultados){
                table+="<tr><td>"+i.nombre
                table+="</td><td>"+i.fecha
                table+="</td></tr>"
            }
            $("#result").html(table);
        }else if(option="Mi Biblioteca"){
            let table="<tr><th>ISBN</th><th>Titulo</th><th>Autor</th><th>Editorial</th></tr>"            
            for(let i of dat.resultados){
                table += "<tr><td>" + i.ISBN 
                table +="</td><td>" + i.titulo 
                table += "</td><td>" + i.autor
                table += "</td><td>"+ i.editorial
                table += "</td></tr>" 
            }
            $("#result").html(table);
        }
    })
})