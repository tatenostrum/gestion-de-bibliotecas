$("#btn-libro").click(function(){
    let ruta;
    
    if($("#isbn").val()==""){
        return false;
    }
    if($(this).hasClass("insertar")){
        ruta="api/libro/create.php";
    }
    if($(this).hasClass("actualizar")){
        ruta="api/libro/update.php";
    }
    if($(this).hasClass("borrar")){
        ruta="api/libro/delete.php";
    }
    if($(this).hasClass("insertar")||$(this).hasClass("actualizar")){
        var datas={
            "isbn":$("#ISBN").val(),
            "titulo":$("#Titulo").val(),
            "autor":$("#Autor").val(),
            "edicion":$("#Edicion").val(),
            "editorial":$("#Editorial").val(),
            "genero":$("#Genero").val(),
            "tapa":$("#Tapa").val(),
            "sinopsis":$("#Sinopsis").val(),
            "fechap":$("#Fechap").val(),
            "fechae":$("#Fechae").val()

        }
    }else if($(this).hasClass("borrar")){
        var datas={
            "isbn":$("#ISBN").val()
        }
    }
    $.post(ruta, datas,(res)=>{
        alert(res.message);
        if(res.message.indexOf("no")==-1){
            $("#ISBN").val("")
            $("#Titulo").val("")
            $("#Autor").val("")
            $("#Edicion").val("")
            $("#Editorial").val("")
            $("#Genero").val("")
            $("#Tapa").val("")
            $("#Sinopsis").val("")
            $("#Fechap").val("")
            $("#Fechae").val("")
            $("#search").val("")
            $("#result").html("")
        }
    })
})