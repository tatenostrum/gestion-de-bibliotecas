-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-10-2018 a las 12:49:24
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdlibros`
--
DROP DATABASE IF EXISTS `bdlibros`;
CREATE DATABASE IF NOT EXISTS `bdlibros` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bdlibros`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor`
--

CREATE TABLE IF NOT EXISTS `autor` (
  `idautor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`idautor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `autor`
--

INSERT INTO `autor` (`idautor`, `nombre`, `fecha`) VALUES
(1, 'Miguel de Cervantes Saavedra', '1547-09-29'),
(2, 'Arturo Perez-Reverte', '1951-01-01'),
(3, 'María Dueñas', '1964-01-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biblioteca`
--

CREATE TABLE IF NOT EXISTS `biblioteca` (
  `iduser` int(11) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `donde` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iduser`,`isbn`),
  KEY `isbn` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libautor`
--

CREATE TABLE IF NOT EXISTS `libautor` (
  `idautor` int(11) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  PRIMARY KEY (`idautor`,`isbn`),
  KEY `isbn` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libautor`
--

INSERT INTO `libautor` (`idautor`, `isbn`) VALUES
(1, '9788493806125'),
(1, '9788494772801'),
(2, '9788420432694'),
(3, '9788408189985');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro`
--

CREATE TABLE IF NOT EXISTS `libro` (
  `isbn` varchar(20) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `edicion` varchar(255) DEFAULT NULL,
  `editorial` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `tapa` varchar(255) DEFAULT NULL,
  `sinopsis` varchar(255) DEFAULT NULL,
  `fechae` date DEFAULT NULL,
  `fechap` date DEFAULT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libro`
--

INSERT INTO `libro` (`isbn`, `titulo`, `edicion`, `editorial`, `genero`, `tapa`, `sinopsis`, `fechae`, `fechap`) VALUES
('9788408189985', 'Las hijas del capitan', '', 'Planeta', '', '', '', '0000-00-00', '0000-00-00'),
('9788420432694', 'Los perros duros no bailan', '', 'Alfaguara', 'Policiaca', 'Dura', '', '0000-00-00', '0000-00-00'),
('9788493806125', 'DON QUIJOTE DE LA MANCHA', '', 'Pluton Ediciones', '', 'Dura', '', '0000-00-00', '0000-00-00'),
('9788494772801', 'DULCINEA DEL TOBOSO', '', 'Linteo', '', '', '', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`iduser`, `user`, `password`, `email`) VALUES
(1, 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com'),
(2, 'Alberto', '177dacb14b34103960ec27ba29bd686b', 'alberto@alberto.com');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  ADD CONSTRAINT `biblioteca_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `usuario` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `biblioteca_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `libro` (`isbn`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `libautor`
--
ALTER TABLE `libautor`
  ADD CONSTRAINT `libautor_ibfk_1` FOREIGN KEY (`idautor`) REFERENCES `autor` (`idautor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `libautor_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `libro` (`isbn`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
