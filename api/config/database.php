<?php
class Database{
    private $host = "localhost";
    private $dbname = "bdlibros_bdlibros";
    private $user = "bdlibros_admin";
    private $pass = "admin";
    public $conn;

    public function getConn(){
        $this->conn = null;
        try{
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->dbname, $this->user, $this->pass);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Error al conectar: ".$exception->getMessage();
        }

        return $this->conn;
    }
}
?>