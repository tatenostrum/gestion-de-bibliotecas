<?php

class Biblioteca{
    
    private $conn;
    private $table_name = "biblioteca";

    public $isbn;
    public $titulo;
    public $autor;
    public $edicion;
    public $editorial;
    public $genero;
    public $tapa;
    public $sinopsis;
    public $fechap;
    public $fechae;
    public $user;

    public function __construct($db){
        $this->conn = $db;
    }

    function clean($st){
        return htmlspecialchars(strip_tags($st));
    }

    function read(){
        $query = "SELECT l.*, a.nombre as autor FROM libro l
                JOIN biblioteca b ON(l.isbn=b.isbn)
                JOIN libautor la ON(l.isbn=la.isbn)
                JOIN autor a ON(a.idautor=la.idautor)
                WHERE b.iduser=:iduser;";
        $stmt = $this->conn->prepare($query);
        $stmt = bindParam(":iduser", $this->clean($this->user));
        $stmt->execute();
        return $stmt;
    }

    function delete(){
        $query = "DELETE FROM biblioteca WHERE iduser=:iduser AND isbn=:isbn;";
        $stmt = $this->conn->prepare($query);
        $stmt = bindParam(":iduser", $this->clean($this->user));
        $stmt = bindParam(":isbn", $this->clean($this->isbn));
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function create(){
        $query = "INSERT INTO biblioteca
                SET
                    iduser=:iduser, isbn=:isbn;";
        $stmt = $this->conn->prepare($query);
        $stmt = bindParam(":iduser", $this->clean($this->user));
        $stmt = bindParam(":isbn", $this->clean($this->isbn));
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}
?>