<?php
class Libro{
    //Variables para la conexion de la base de datos y la tabla
    private $conn;
    private $table_name = "libro";

    //Propiedades del objeto
    public $isbn;
    public $titulo;
    public $autor;
    public $edicion;
    public $editorial;
    public $genero;
    public $tapa;
    public $sinopsis;
    public $fechap;
    public $fechae;

    //Constructor de la clase
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){
        $query = "SELECT l.*, a.nombre AS autor
                    FROM libro l 
                    JOIN libautor la ON(l.isbn=la.isbn)
                    JOIN autor a ON(a.idautor=la.idautor);";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function clean($st){
        return htmlspecialchars(strip_tags($st));
    }

    function create(){

        $query = "INSERT INTO libro
                SET
                    isbn=:isbn, titulo=:titulo, edicion=:edicion, editorial=:editorial, genero=:genero, tapa=:tapa, sinopsis=:sinopsis, fechap=:fechap, fechae=:fechae;";

        $stmt = $this->conn->prepare($query);
        
        $stmt->bindParam(":isbn", $this->clean($this->isbn));
        $stmt->bindParam(":titulo", $this->clean($this->titulo));
        $stmt->bindParam(":edicion", $this->clean($this->edicion));
        $stmt->bindParam(":editorial", $this->clean($this->editorial));
        $stmt->bindParam(":genero", $this->clean($this->genero));
        $stmt->bindParam(":tapa", $this->clean($this->tapa));
        $stmt->bindParam(":sinopsis", $this->clean($this->sinopsis));
        $stmt->bindParam(":fechap", $this->clean($this->fechap));
        $stmt->bindParam(":fechae", $this->clean($this->fechae));
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function createlibautor(){
        $query ="INSERT INTO libautor
                SET
                    isbn=:isbn, idautor=:autor;";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":isbn", $this->clean($this->isbn));
        $stmt->bindParam(":autor", $this->clean($this->autor));
        if($stmt->execute()){
            return true;
        }

        return false;

    }

    function readOne(){
        $query = "SELECT l.*, a.nombre AS autor
                    FROM libro l 
                    JOIN libautor la ON(l.isbn=la.isbn)
                    JOIN autor a ON(a.idautor=la.idautor)
                    WHERE l.isbn=:isbn;";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":isbn", $this->isbn);

        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->isbn=$row['isbn'];
        $this->titulo=$row['titulo'];
        $this->autor=$row['autor'];
        $this->edicion=$row['edicion'];
        $this->editorial=$row['editorial'];
        $this->genero=$row['genero'];
        $this->tapa=$row['tapa'];
        $this->sinopsis=$row['sinopsis'];
        $this->fechap=$row['fechap'];
        $this->fechae=$row['fechae'];
    }

    function update(){
        $query = "UPDATE libro
                SET
                    titulo=:titulo, edicion=:edicion, editorial=:editorial, genero=:genero, tapa=:tapa, sinopsis=:sinopsis, fechap=:fechap, fechae=:fechae
                WHERE
                    isbn=:isbn;";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":isbn", $this->clean($this->isbn));
        $stmt->bindParam(":titulo", $this->clean($this->titulo));
        $stmt->bindParam(":edicion", $this->clean($this->edicion));
        $stmt->bindParam(":editorial", $this->clean($this->editorial));
        $stmt->bindParam(":genero", $this->clean($this->genero));
        $stmt->bindParam(":tapa", $this->clean($this->tapa));
        $stmt->bindParam(":sinopsis", $this->clean($this->sinopsis));
        $stmt->bindParam(":fechap", $this->clean($this->fechap));
        $stmt->bindParam(":fechae", $this->clean($this->fechae));
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function delete(){
        $query = "
                DELETE FROM libautor
                WHERE isbn=:isbn;";
        $query .= "DELETE FROM libro
                WHERE isbn=:isbn;";
        
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":isbn", $this->clean($this->isbn));

        if($stmt->execute()){
            return true;
        }

        return false;

    }

    function search(){
        $query = 'SELECT l.*, a.nombre AS autor
                    FROM libro l 
                    JOIN libautor la ON(l.isbn=la.isbn)
                    JOIN autor a ON(a.idautor=la.idautor)
                    WHERE l.isbn LIKE "%'.$this->clean($this->isbn).'%";';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

}
?>