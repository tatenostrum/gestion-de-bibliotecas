<?php
error_reporting(0);
class Autor{
    
    private $conn;
    private $table_name = "autor";

    public $idautor;
    public $nombre;
    public $fecha;

    public function __construct($db){
        $this->conn = $db;
    }

    function read(){
        $query = "SELECT idautor, nombre, fecha
                    FROM autor;";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function clean($st){
        return htmlspecialchars(strip_tags($st));
    }

    function create(){
        $query = "INSERT INTO autor
                SET
                    nombre=:nombre, fecha=:fecha;";
        
        $stmt = $this->conn->prepare($query);
        
        $stmt->bindParam(":nombre", $this->clean($this->nombre));
        $stmt->bindParam(":fecha", $this->clean($this->fecha));

        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function readOne(){
        $query = "SELECT *
                    FROM autor a
                    WHERE a.idautor=:idautor ;";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idautor', $this->clean($this->idautor));
        $stmt->execute();

        $row = $stmt->fetch();
        $this->idautor=$row['idautor'];
        $this->nombre=$row['nombre'];
        $this->fecha=$row['fecha'];
    }

    function update(){
        $query = 'UPDATE autor
                SET
                    nombre=:nombre, fecha=:fecha
                WHERE
                    idautor=:idautor';
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idautor', $this->clean($this->idautor));
        $stmt->bindParam(':nombre', $this->clean($this->nombre));
        $stmt->bindParam(':fecha', $this->clean($this->fecha));
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function delete(){
        $query = "DELETE FROM autor
                WHERE idautor=:idautor;";
        
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":idautor", $this->clean($this->idautor));

        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function search(){
        $query = 'SELECT idautor, nombre, fecha
                    FROM autor
                    WHERE nombre LIKE "%'.$this->clean($this->nombre).'%";';
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}
?>