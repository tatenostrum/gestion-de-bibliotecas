<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/database.php';
include_once '../objects/biblioteca.php';

$database = new Database();
$db = $database->getConn();

$biblioteca = new Biblioteca($db);

$biblioteca->user=isset($_GET['iduser']) ? $_GET['iduser'] : die();

$stmt = $biblioteca->read();
$num = $stmt->rowCount();
if($num>0){
    $arbibliotecas=array();
    $arbibliotecas["resultados"]=array();
    while($fila = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($fila);
        $item=array(
            "isbn"=>$isbn,
            "titulo" => $titulo,
            "autor" => $autor,
            "edicion" => $edicion,
            "editorial" => $editorial,
            "sinopsis" => $sinopsis,
            "tapa" => $tapa,
            "genero" => $genero,
            "fechap" => $fechap,
            "fechae" => $fechae
        );

        array_push($arbibliotecas["resultados"],$item);
    }
    echo json_encode($arbibliotecas);
}else{
    echo json_encode(array("message"=>"No se han encontrado isbn"));
}
?>