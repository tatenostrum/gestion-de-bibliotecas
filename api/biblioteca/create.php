<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$method = $_SERVER['REQUEST_METHOD'];
if($method!="POST"||$_POST['iduser']==""||$_POST['isbn']==""){
    die();
}
include_once '../config/database.php';
include_once '../objects/biblioteca.php';

$database = new Database();
$db = $database->getConn();

$biblioteca = new Biblioteca($db);

$biblioteca->user= $_POST['user'];
$biblioteca->isbn=$_POST['isbn'];

if($biblioteca->create()){
    echo '{ "message" : "Se ha introducido correctamente el registro"}';
}else{
    echo '{ "message" : "Error no se ha introducido correctamente el registro"}';
}

?>