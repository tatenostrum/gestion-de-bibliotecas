<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include_once '../config/database.php';
include_once '../objects/biblioteca.php';

$database = new Database();
$db = $database->getConn();

$biblioteca = new biblioteca($db);


$biblioteca->isbn=$_POST['isbn'];
$biblioteca->user=$_POST['user'];

if($biblioteca->delete()){
    echo '{"message" : "El registro se ha borrado"}';
}else{
    echo '{"message" : "El registro no se ha borrado"}';
}

?>