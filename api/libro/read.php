<?php
// Headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//Incluimos base de datos y objetos necesarios
include_once '../config/database.php';
include_once '../objects/libro.php';

//Iniciamos la conexion a la base de datos
$database = new Database();
$db = $database->getConn();

//Iniciamos el objeto
$libro = new Libro($db);
$stmt = $libro->read();
$num = $stmt->rowCount();

if($num>0){
    $arlibros=array();
    $arlibros["resultados"]=array();

    while($fila = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($fila);

        $item=array(
            "ISBN" => $isbn,
            "titulo" => $titulo,
            "autor" => $autor,
            "edicion" => $edicion,
            "editorial" => $editorial,
            "sinopsis" => $sinopsis,
            "tapa" => $tapa,
            "genero" => $genero,
            "fechap" => $fechap,
            "fechae" => $fechae
        );

        array_push($arlibros["resultados"], $item);
    }

    echo json_encode($arlibros);

}else{
    echo json_encode(
        array("message" => "No se han encontrado libros")
    );
}
?>