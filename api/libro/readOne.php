<?php
error_reporting(0);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/database.php';
include_once '../objects/libro.php';

$database = new Database();
$db = $database->getConn();

$libro = new Libro($db);
$libro->isbn=isset($_GET['isbn']) ? $_GET['isbn'] : die();

$libro->readOne();

$arlibro = array(
    "isbn" => $libro->isbn,
    "titulo" => $libro->titulo,
    "autor" => $libro->autor,
    "edicion" => $libro->edicion,
    "editorial" => $libro->editorial,
    "sinopsis" => $libro->sinopsis,
    "tapa" => $libro->tapa,
    "genero" => $libro->genero,
    "fechap" => $libro->fechap,
    "fechae" => $libro->fechae
);
echo json_encode($arlibro);
?>