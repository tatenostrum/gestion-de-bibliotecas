<?php
error_reporting(0);
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$method = $_SERVER['REQUEST_METHOD'];
include_once '../config/database.php';
include_once '../objects/libro.php';

$database = new Database();
$db = $database->getConn();

$libro = new Libro($db);
if($method!="POST"||$_POST['isbn']==""){
    die();
}

$libro->isbn=$_POST['isbn'];
$libro->titulo=$_POST['titulo'];
$libro->autor=$_POST['autor'];
$libro->edicion=$_POST['edicion'];
$libro->editorial=$_POST['editorial'];
$libro->genero=$_POST['genero'];
$libro->tapa=$_POST['tapa'];
$libro->sinopsis=$_POST['sinopsis'];
$libro->fechap=$_POST['fechap'];
$libro->fechae=$_POST['fechae'];
if($libro->create()&&$libro->createlibautor()){
    echo json_encode(array("message" => "Se ha introducido correctamente el libro"));
}else{
    echo json_encode(array("message" => "No se ha introducido correctamente el libro"));
}
?>