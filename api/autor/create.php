<?php
error_reporting(0);
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$method = $_SERVER['REQUEST_METHOD'];
if($method!="POST"||$_POST['nombre']==""){
    die();
}
include_once '../config/database.php';
include_once '../objects/autor.php';

$database = new Database();
$db = $database->getConn();

$autor = new Autor($db);

$autor->nombre= $_POST['nombre'];
$autor->fecha=$_POST['fecha'];

if($autor->create()){
    echo json_encode(array("message" => "Se ha introducido correctamente el autor"));
}else{
    echo json_encode(array("message" => "No se ha introducido correctamente el autor"));
}

?>