<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include_once '../config/database.php';
include_once '../objects/autor.php';

$database = new Database();
$db = $database->getConn();

$autor = new Autor($db);


$autor->idautor=$_POST['idautor'];

if($autor->delete()){
    echo '{"message" : "El autor se ha borrado"}';
}else{
    echo '{"message" : "El autor no se ha borrado"}';
}

?>