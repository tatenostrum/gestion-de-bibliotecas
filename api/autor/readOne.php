<?php
error_reporting(0);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/database.php';
include_once '../objects/autor.php';

$database = new Database();
$db = $database->getConn();

$autor = new Autor($db);

$autor->idautor=isset($_GET['idautor']) ? $_GET['idautor'] : die();

$autor->readOne();

$arautor = array(
    "idautor" => $autor->idautor,
    "nombre" => $autor->nombre,
    "fecha" => $autor->fecha,
    
);
echo json_encode($arautor);
?>