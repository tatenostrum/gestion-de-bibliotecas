<?php

// Headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//Incluimos base de datos y objetos necesarios
include_once '../config/database.php';
include_once '../objects/autor.php';

//Iniciamos la conexion a la base de datos
$database = new Database();
$db = $database->getConn();

//Iniciamos el objeto
$autor = new Autor($db);
$stmt = $autor->read();
$num = $stmt->rowCount();

if($num>0){
    $arautores=array();
    $arautores["resultados"]=array();

    while($fila = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($fila);

        $item=array(
            "idautor"=>$idautor,
            "nombre"=>$nombre,
            "fecha"=>$fecha
        );

        array_push($arautores["resultados"], $item);
    }

    echo json_encode($arautores);

}else{
    echo json_encode(
        array("message" => "No se han encontrado autores")
    );
}