<?php
	session_start();
	if (session_status()!= PHP_SESSION_ACTIVE || !isset($_SESSION["iduser"])){
		header('Location: index.php');
	}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="Martí Masot">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style>
            h1{
                text-align: center;
            }
            .forma{
                text-align: center;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/jquery-3.3.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1>Base de Dades d'Autors, Llibres i Col·leccions.<br><small>Hola <?php echo $_SESSION["user"] ?></small></h1>
            </div>
            <article class="row">
                <nav class="col-sm-2">
                    <ul class="nav flex-column nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="main.php">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Libro</a>
                            <div class="dropdown-menu">
                                <a href="insertarlibro.php" class="active  dropdown-item">Insertar</a>
                                <a href="modificarlibro.php" class="dropdown-item">Modificar</a>
                                <a href="borrarlibro.php" class="dropdown-item">Borrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Autor</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="insertarautor.php">Insertar</a>
                            <a href="modificarautor.php" class="dropdown-item">Modificar</a>
                            <a class="dropdown-item" href="borrarautor.php">Borrar</a>
                        </div>
                    </li>
                        <li class="nav-item">
				            <a class="nav-link" href="api/logout.php">Cerrar Sesión</a>
                        </li>
                    </ul>
                </nav>
                <section class="col-sm-6">
                    <div class="forma">
                        <div class="form-group row">
                            <label for="ISBN" class="col-2 col-form-label">ISBN</label>
                            <div class="col-10">
                                <input class="form-control" id="ISBN" type="text" name="ISBN" placeholder="ISBN">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Titulo" class="col-2 col-form-label">Título *</label>
                            <div class="col-10">
                                <input class="form-control" id="Titulo" type="text" name="titulo" placeholder="Título">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Autor" class="col-2 col-form-label">Autor *</label>
                            <div class="col-10">
                                <input class="form-control" id="Autor" type="text" name="autor" placeholder="Autor">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Editorial" class="col-2 col-form-label">Editorial *</label>
                            <div class="col-10">
                                <input class="form-control" id="Editorial" type="text" name="editorial" placeholder="Editorial">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Edicion" class="col-2 col-form-label">Edición</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="Edicion" name="edicion" placeholder="Edición">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Genero" class="col-2 col-form-label">Género</label>
                            <div class="col-10">
                                <input id="Genero" class="form-control" type="text" name="genero" placeholder="Género">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Tapa" class="col-2 col-form-label">Tapa</label>
                            <div class="col-10">
                                <input id="Tapa" class="form-control" type="text" name="tapa" placeholder="Tapa">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Sinopsis" class="col-2 col-form-label">Sinopsis</label>
                            <div class="col-10">
                                <input class="form-control" id="Sinopsis" type="text_area" name="sinopsis" placeholder="Sinopsis">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="FechaP" class="col-2 col-form-label">Fecha de publicación</label>
                            <div class="col-10">
                                <input class="form-control" id="FechaP" type="date" name="Fechap">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="FechaE" class="col-2 col-form-label">Fecha de edición</label>
                            <div class="col-10">
                                <input class="form-control" id="FechaE" type="date" name="Fechae">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button id="btn-libro" class="btn btn-primary insertar">Añadir libro</button>
                            </div>
                        </div>
                    </div>
                </section>
                <aside class="col-sm-4">
                    <div class="container-fluid">
                        <div class="input-group">
                            <input class="form-control libautor" id="search" type="search" placeholder="Buscar Autor...">
                        </div>
                        <table class="table table-striped" id="result"></div>
                    </div>
                </aside>
            </article>
        </div>
        <script src="js/buscar.js"></script>
        <script src="js/libro.js"></script>
    </body>
</html>