<?php
session_start();
if (session_status()!= PHP_SESSION_ACTIVE || !isset($_SESSION["iduser"])){
	header('Location: index.php');
}
?>
<html lang="es">
    <head>
        <title>Gestión de bibliotecas</title>
        <meta charset="utf-8">
        <meta name="author" content="Martí Masot">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/jquery-3.3.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
    <div class="container-fluid">
        <div class="jumbotron text-center">
            <h1>Base de Dades d'Autors, Llibres i Col·leccions.<br><small>Hola <?php echo $_SESSION["user"] ?></small></h1>
        </div>
        <main>
            <div class="row">
                <nav class="col-sm-2">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Libro</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="insertarlibro.php">Añadir</a>
                                <a class="dropdown-item" href="modificarlibro.php">Actualizar</a>
                                <a class="dropdown-item" href="borrarlibro.php">Borrar</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Autor</a>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="insertarautor.php">Añadir</a>
                                <a class="dropdown-item" href="modificarautor.php">Actualizar</a>
                                <a class="dropdown-item" href="borrarautor.php">Borrar</a>
                            </div>
                        </li>
                        <li>
                            <a class="nav-link" href="api/logout.php">Logout</a>
                        </li>
                    </ul>
                </nav>
                <article class="col-sm-7">            	
                <div class="row">
                    <div class="col-*-*">
                        <select class="form-control" id="sel">
                            <option value="libro">Libros</option>
                            <option value="autor">Autor</option>
                            <option value="biblio" disabled>Mi biblioteca</option>
                        </select>
                    </div>
                </div>
                <table class="table" id="result"></table>
                </article>
                <aside class="col-sm-3">
                
                </aside>
			</div>
        </main>
        </div> 
		

        <script src="js/mostrar.js"></script>
    </body>
</html>