<?php
	session_start();
	if (session_status()!= PHP_SESSION_ACTIVE || !isset($_SESSION["iduser"])){
		header('Location: index.php');
	}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="Martí Masot">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style>
            h1{
                text-align: center;
            }
            .forma{
                text-align: center;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/jquery-3.3.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        
        <div class="container-fluid">
            <div class="jumbotron">
                <h1>Base de Dades d'Autors, Llibres i Col·leccions.<br><small>Hola <?php echo $_SESSION["user"] ?></small></h1>
            </div>
            <article class="row">
                <nav class="col-sm-2">
                <ul class="nav flex-column nav-pills">
                        <li class="nav-item">
                            <a class="nav-link" href="main.php">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Libro</a>
                            <div class="dropdown-menu">
                                <a href="insertarlibro.php" class="dropdown-item">Insertar</a>
                                <a href="modificarlibro.php" class="dropdown-item">Modificar</a>
                                <a href="borrarlibro.php" class="dropdown-item">Borrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Autor</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item active" href="insertarautor.php">Insertar</a>
                            <a href="modificarautor.php" class="dropdown-item">Modificar</a>
                            <a class="dropdown-item" href="borrarautor.php">Borrar</a>
                        </div>
                    </li>
                        <li class="nav-item">
				            <a class="nav-link" href="api/logout.php">Cerrar Sesión</a>
                        </li>
                    </ul>
                </nav>
                <section class="col-sm-7" >
                    <div class="forma">
                        <div class="form-group row">
                            <label for="nombre" class="col-2 col-form-label">Nombre</label>
                            <div class="col-10">
                                
                                <input class="form-control" id="nombre" type="text" name="Nombre" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fecha" class="col-2 col-form-label">Fecha</label>
                            <div class="col-10">
                                <input class="form-control" id="fecha" type="date" name="Fecha">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-12">
                                <button class="insertar btn btn-primary" id="btn-autor">Añadir Autor</button>
                            </div>
                        </div>
                        </div>
                </section>
                <aside class="col-sm-3">
                

                </aside>
            </article>
        </div>
        <script src="js/autor.js"></script>
    </body>
</html>