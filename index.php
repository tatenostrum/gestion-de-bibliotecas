<?php 
	session_start();
	if (isset($_SESSION["iduser"])){
		header("Location: main.php");
	}
?>
<html lang="es">
    <head>
        <title>Gestión de bibliotecas</title>
        <meta charset="utf-8">
        <meta name="author" content="Martí Masot">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    </head>
    <body>
        <div class="jumbotron text-center">
        <h1>Base de Dades d'Autors, Llibres i Col·leccions.</h1>
        </div>
        <main class="container">
            <h2 class="text-center">Login</h2>
            <article class="row">
                <div class="col-sm-3"></div>
                <form class="col-sm-6 form-horizontal" role="form" method="POST" action="api/login.php">
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 control-label">Email </label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pass" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="pass" name="pass" placeholder="pass">
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary row col-sm-12" name="Login">
                </form>
                
                <div class="col-sm-3"></div>
            </article>
        </main>
        <script src="js/jquery-3.3.1.js"></script>
    </body>
</html>